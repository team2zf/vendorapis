<?php use common\components\BaseVendorApi;
class Vendor extends BaseVendorApi
{

    public $user = 'API_Tests';
    public $pass = 'api_test';

    public function init()
    {
        $this->baseUrl = 'http://api.medexltd.com';
    }

    protected function getHttpClient()
    {
        if (!is_object($this->_httpClient)) {
            parent::getHttpClient();
            $this->_httpClient->formatters = [
                yii\httpclient\Client::FORMAT_XML => [
                    'class' => 'yii\httpclient\XmlFormatter',
                    'rootTag' => 'mdx',
                    'itemTag' => 'product',
                ]
            ];
        }

        return $this->_httpClient;
    }

    /**
     * @param common\models\ShippingItemModel $shipping
     * @return string
     */
    public function exportItem($shipping)
    {
        $error = null;

        if ($shipping->type == \common\models\enumerables\ShippingType::NORMAL) {
            $products = [];
            foreach ($shipping->orderItemShippings as $item)
            {
                $products[] = [
                    'product_id' => $item->getVendorProductByVendorId($shipping->vendorId)->name,
                    'quantity' => $item->quantity,
                ];
            }
            $this->setParams([
                'authentication' => [
                    'username' => $this->user,
                    'password' => $this->pass,
                ],
                'orderdata' => [
                    'order_customer' => $shipping->order->firstName . ' ' . $shipping->order->lastName,
                    'order_address' => $shipping->order->shippingAddressStreet,
                    'order_state' => $shipping->order->shippingAddressState ?: '-',
                    'order_number' => $shipping->id,
                    'order_city' => $shipping->order->shippingAddressCity,
                    'order_country' => $shipping->order->shippingAddressCountry,
                    'order_zip' => $shipping->order->shippingAddressZip ?: '-',
                    'shipping_id' => $shipping->vendorShipping->name,
                    'products' => $products,
                ]
            ]);
        } else {
            $this->setParams([
                'authentication' => [
                    'username' => $this->user,
                    'password' => $this->pass,
                ],
                'order_number' => $shipping->prevAttempts[0]->id,
                'comment' => $shipping->vendorComment . ' ' . $model->getLastAttemptDiff(),
            ]);
        }

        $this->request('webservicexml/sendOrder');

        if ($error = $this->getResponseValue('error')) {
            $this->_errors = $this->_response;
            $error = $this->getErrorMessage();
        }

        parent::exportItem($shipping);

        return $error;
    }

    public function getErrorMessage()
    {
        $error = $this->getResponseValue('error');
        if ($error) {
            return $error['error_code'] . ' - ' . $error['error_str'];
        } else {
            return null;
        }
    }

    public function getTrackingNumber($id)
    {
        $this->setParams([
            'authentication' => [
                'username' => $this->user,
                'password' => $this->pass,
            ],
            'order_number' => $id,
        ]);

        $this->request('webservicexml/getTrackings');

        if ($error = $this->getErrorMessage()) {
            $this->_errors = $this->_response;
            echo $id . ': ' . $error . PHP_EOL;
        } else {
            $trackings = $this->getResponseValue('trackings');
            if (isset($trackings[0])) {
                echo $id . ': ' . implode(', ', $trackings) . PHP_EOL;
                return $trackings;
            } else {
                echo $id . ': empty number' . PHP_EOL;
                return null;
            }
        }
    }

    public function importCallback($data)
    {
        $xml = simplexml_load_string($data);
        if (isset($xml->order_number) && isset($xml->trackings[0]->tracking_id)) {
            $id = (string)$xml->order_number;
            $this->updateTrackingNumber($id, (array)$xml->trackings->tracking_id);
        }
        echo 'ok';
    }

}